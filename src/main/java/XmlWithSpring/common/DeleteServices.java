package XmlWithSpring.common;

import XmlWithSpring.xml.XmlReader;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.*;

@Service
public class DeleteServices {

    private final static Logger logger = Logger.getLogger(XmlReader.class);

    public boolean deleteFile(String fileBasePath, String fileName) {

        Path filePath = getFilePath(fileBasePath, fileName);

        try {
            Files.delete(filePath);
            logger.info("The file was deleted.");
        }catch (NoSuchFileException x){
            x.getMessage();
            logger.info("The file was not found.");
            return false;
        }catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Path getFilePath(String fileBasePath, String fileName) {
        return FileSystems.getDefault().getPath(fileBasePath, fileName);
    }
}
