package XmlWithSpring.common;

import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import XmlWithSpring.xml.XmlReader;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerFilterSearchService {

    private final static Logger logger = Logger.getLogger(XmlReader.class);

    public Customer getCustomerByName(String name, Customers customers) {
        List<Customer> customerList = customers.getCustomerList();
        Optional<Customer> customer = customerList.stream()
                                                    .filter(searchedCustomer -> name.equalsIgnoreCase(searchedCustomer.getName()))
                                                    .findFirst();

        if (!customer.isPresent()) {
            logger.info("findCustomerByName method resulted in customer=null");
            return null;
        }
        return customer.get();
    }

    public Customers getCustomersByCity(String city, Customers customers) {
        List<Customer> customerList = customers.getCustomerList();
        List<Customer> customersByCity = customerList.stream()
                                                        .filter(customer -> city.equalsIgnoreCase(customer.getCity()))
                                                        .collect(Collectors.toList());

        customers.setCustomerList(customersByCity);
        return customers;
    }

}
