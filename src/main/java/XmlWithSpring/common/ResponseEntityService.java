package XmlWithSpring.common;

import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import XmlWithSpring.model.Files;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ResponseEntityService {
    public ResponseEntity<Customers> getCustomersResponseEntity(Customers customers) {
        HttpHeaders headers = addHttpHeaders();
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(customers);
    }

    public ResponseEntity<Customer> getCustomerResponseEntity(Customer customer) {
        HttpHeaders headers = addHttpHeaders();
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(customer);
    }

    public ResponseEntity<Files> getFilesResponseEntity(Files files) {
        HttpHeaders headers = addHttpHeaders();
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(files);
    }

    public ResponseEntity<Void> deleteFilesResponseEntity(Boolean fileDeleted) {

        if (fileDeleted) {
            return ResponseEntity
                    .ok()
                    .build();
        }
        else {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    private HttpHeaders addHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        return headers;
    }
}
