package XmlWithSpring.excel;

import XmlWithSpring.common.CustomerFilterSearchService;
import XmlWithSpring.xml.XmlReader;
import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ExcelReader {

    @Autowired
    private CustomerFilterSearchService customerFilterSearchService;


    Customers readFromXlsxFile(String excelFilePath) {

        Customers customersToReturn = new Customers();
        List<Customer> customerList = new ArrayList<>();

        try (FileInputStream inputStream = new FileInputStream(new File(excelFilePath))){

            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();

            customerList = readExcelRow(customerList, iterator);
            workbook.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        customersToReturn.setCustomerList(customerList);
        return customersToReturn;
    }

    private List<Customer> readExcelRow(List<Customer> customerList, Iterator<Row> iterator) {
        while (iterator.hasNext()){
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            Customer customer = new Customer();
            customer = readFieldDetailsFromEachCell(cellIterator, customer);
            customerList.add(customer);
        }
        return customerList;
    }

    private Customer readFieldDetailsFromEachCell(Iterator<Cell> cellIterator, Customer customer) {
        while (cellIterator.hasNext()){
            Cell nextCell = cellIterator.next();
            int columnIndex = nextCell.getColumnIndex();

            switch (columnIndex) {
                case 0:
                    customer.setName((String) getCellValue(nextCell));
                    break;
                case 1:
                    customer.setLastName((String) getCellValue(nextCell));
                    break;
                case 2:
                    customer.setAge( ((Double)getCellValue(nextCell)).intValue() );
                    break;
                case 3:
                    customer.setCity((String) getCellValue(nextCell));
                    break;
                case 4:
                    customer.setPhone( (Double) getCellValue(nextCell) );
                    break;
                case 5:
                    customer.setEmail((String) getCellValue(nextCell));
                    break;
            }
        }
        return customer;
    }

    private Object getCellValue(Cell cell) {
        switch (cell.getCellTypeEnum()){
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case NUMERIC:
                return cell.getNumericCellValue();
        }
        return null;
    }


    public Customer findCustomerByName(String pathName, String name) {
        Customers customers = readFromXlsxFile(pathName);
        return customerFilterSearchService.getCustomerByName(name, customers);
    }

    public Customers findCustomersByCity(String pathName, String city) {
        Customers customers = readFromXlsxFile(pathName);
        return customerFilterSearchService.getCustomersByCity(city, customers);
    }

}
