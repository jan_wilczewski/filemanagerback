package XmlWithSpring.excel;

import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import XmlWithSpring.model.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@Service
public class ExcelService {

    private String fileBasePath = "ExcelFileBase\\";

    @Autowired
    private ExcelWriter excelWriter;
    @Autowired
    private ExcelReader excelReader;


    Files getFilesAvailableToBeRead(String folderName) {

        Files files = new Files();
        File[] filesTable = getFilesFromDirectory(folderName);
        setListOfFilesNames(filesTable, files);
        return files;
    }

    private void setListOfFilesNames(File[] filesTable, Files files) {
        List<String> filesString = files.getFiles();
        for (File file: filesTable){
            filesString.add(file.getName());
        }
        files.setFiles(filesString);
    }

    private File[] getFilesFromDirectory(String folderName) {
        File directory = new File(folderName);
        return directory.listFiles();
    }

    Customers getCustomersByFileType(String file, Customers customers) throws FileNotFoundException {

        if (file.endsWith(".xlsx")) {
            customers = excelReader.readFromXlsxFile(fileBasePath + file);
            return customers;
        }
        throw new FileNotFoundException();
    }

    Customer getCustomerByNameByFileType(String file, String name) throws FileNotFoundException {

        if (file.endsWith(".xlsx")) {
            Customer customer = excelReader.findCustomerByName(fileBasePath + file, name);
            return customer;
        }
        throw new FileNotFoundException();
    }

    Customers getCustomersByCityByFileType(String file, String city) throws FileNotFoundException {

        if (file.endsWith(".xlsx")) {
            Customers customers = excelReader.findCustomersByCity(fileBasePath + file, city);
            return customers;
        }
        throw new FileNotFoundException();
    }

    void saveExcelFileToDisc(String file, Customers customers) throws IllegalAccessException {

        if (file.endsWith(".xlsx")) {
            excelWriter.createExcelFile(fileBasePath + file, customers);
        }
        else {
            throw new IllegalAccessException();
        }
    }

    void updateExcelFile(String file, Customers customers) throws IllegalAccessException {

        if (file.endsWith(".xlsx")) {
            excelWriter.createExcelFile(fileBasePath + file, customers);
        }
        else {
            throw new IllegalAccessException();
        }
    }
}
