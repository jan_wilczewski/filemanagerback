package XmlWithSpring.excel;


import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static java.lang.System.out;

@Component
public class ExcelWriter {

    public void createExcelFile(String fileName, Customers customers) {

        List<Customer> customerList = customers.getCustomerList();

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("sheet1");

            int rowNumber = 0;
            for (Customer customer: customerList) {
                Row row = sheet.createRow(rowNumber++);
                createList(customer, row);
            }

            saveExcelFileToDisc(fileName, workbook);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createList(Customer customer, Row row) {
        Cell cell = row.createCell(0);
        cell.setCellValue(customer.getName());

        cell = row.createCell(1);
        cell.setCellValue(customer.getLastName());

        cell = row.createCell(2);
        cell.setCellValue(customer.getAge());

        cell = row.createCell(3);
        cell.setCellValue(customer.getCity());

        cell = row.createCell(4);
        cell.setCellValue(customer.getPhone());

        cell = row.createCell(5);
        cell.setCellValue(customer.getEmail());
    }

    private void saveExcelFileToDisc(String fileName, XSSFWorkbook workbook) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(fileName));
        workbook.write(fos);
        out.close();
    }
}


//http://findnerd.com/list/view/Export-from-a-list-containing-objects-to-excel-file-in-java/17491/