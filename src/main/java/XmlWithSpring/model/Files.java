package XmlWithSpring.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Data
public class Files {

    private List<String> files = new ArrayList<>();

}
