package XmlWithSpring.model;


import lombok.Data;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement(name = "customers")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Customers {

    @XmlElement(name = "customer")
    private List<Customer> customerList = null;

}