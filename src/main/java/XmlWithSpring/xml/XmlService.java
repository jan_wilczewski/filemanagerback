package XmlWithSpring.xml;

import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import XmlWithSpring.model.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@Service
public class XmlService {

    private String fileBasePath = "XmlFileBase\\";

    @Autowired
    private XmlWriter xmlWriter;
    @Autowired
    private XmlReader xmlReader;


    Files getFilesAvailableToBeRead(String folderName) {

        Files files = new Files();
        File[] filesTable = getFilesFromDirectory(folderName);
        setListOfFilesNames(filesTable, files);
        return files;
    }

    private void setListOfFilesNames(File[] filesTable, Files files) {
        List<String> filesString = files.getFiles();
        for (File file: filesTable){
            filesString.add(file.getName());
        }
        files.setFiles(filesString);
    }

    private File[] getFilesFromDirectory(String folderName) {
        File directory = new File(folderName);
        return directory.listFiles();
    }


    Customers getCustomersByFileType(String file, Customers customers) throws FileNotFoundException {

        if(file.endsWith(".xml")) {
            customers = xmlReader.readFromXmlFile(fileBasePath + file);
            return customers;
        }
        throw new FileNotFoundException();
    }

    Customer getCustomerByNameByFileType(String file, String name) throws FileNotFoundException {
        if(file.endsWith(".xml")) {
            Customer customer = xmlReader.findCustomerByName(fileBasePath + file, name);
            return customer;
        }
        throw new FileNotFoundException();
    }

    Customers getCustomersByCityByFileType(String file, String city) throws FileNotFoundException {
        if(file.endsWith(".xml")) {
            Customers customers = xmlReader.findCustomersByCity2(fileBasePath + file, city);
            return customers;
        }
        throw new FileNotFoundException();
    }

    void saveXmlFileToDisc(String file, Customers customers) throws IllegalAccessException {

        if (file.endsWith(".xml")) {
            xmlWriter.saveItemsListToXmlFile(fileBasePath + file, customers);
        }
        throw new IllegalAccessException();

    }

    void updateXmlFile(String file, Customers customers) throws IllegalAccessException {

        if (file.endsWith(".xml")) {
            xmlWriter.saveItemsListToXmlFile(fileBasePath + file, customers);
        } else {
            throw new IllegalAccessException();
        }
    }
}
