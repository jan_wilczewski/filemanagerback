package XmlWithSpring.xml;

import XmlWithSpring.common.CustomerFilterSearchService;
import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class XmlReader {

    private final static Logger logger = Logger.getLogger(XmlReader.class);

    @Autowired
    private CustomerFilterSearchService customerFilterSearchService;


    Customers readFromXmlFile(String filePath){
        Customers customersToReturn = null;
        try {

            File file = new File(filePath);

            JAXBContext jaxbContext = JAXBContext.newInstance(Customers.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            customersToReturn = (Customers) jaxbUnmarshaller.unmarshal(file);

            if (logger.isInfoEnabled()) {
                logger.info("This is INFO log from readFromXmlFile method success.");
            }

        } catch (JAXBException e) {
            logger.error("This is logger error from readFromXmlFile method: " + e);
        }

        return customersToReturn;
    }


    Customer findCustomerByName(String pathName, String name) {
        Customers customers = readFromXmlFile(pathName);
        return customerFilterSearchService.getCustomerByName(name, customers);
    }


    Customers findCustomersByCity2(String pathName, String city) {
        Customers customers = readFromXmlFile(pathName);
        return customerFilterSearchService.getCustomersByCity(city, customers);
    }



}
