package XmlWithSpring.xml;

import XmlWithSpring.common.DeleteServices;
import XmlWithSpring.common.ResponseEntityService;
import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import XmlWithSpring.model.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.io.FileNotFoundException;

@Controller
@RequestMapping("/xml")
public class XmlController {

    @Autowired
    private XmlService xmlService;
    @Autowired
    private DeleteServices deleteServices;
    @Autowired
    private ResponseEntityService responseEntityService;

    @GetMapping("/files")
    public ResponseEntity<Files> getFiles(@RequestParam("folder") String folderName) {
        Files files = xmlService.getFilesAvailableToBeRead(folderName);
        return responseEntityService.getFilesResponseEntity(files);
    }

    @GetMapping("/customers")
    public ResponseEntity<Customers> getCustomers(@RequestParam("file") String file) throws FileNotFoundException {
        Customers customers = new Customers();
        customers = xmlService.getCustomersByFileType(file, customers);
        return responseEntityService.getCustomersResponseEntity(customers);
    }

    @GetMapping("/customer/name")
    public ResponseEntity <Customer> getCustomerByName(@RequestParam("file") String file, @RequestParam("name") String name) throws FileNotFoundException {

        Customer customer = xmlService.getCustomerByNameByFileType(file, name);
        return responseEntityService.getCustomerResponseEntity(customer);
    }


    @GetMapping("/customer/city")
    public ResponseEntity <Customers> getCustomerByCity(@RequestParam("file") String file, @RequestParam("city") String city) throws FileNotFoundException {

        Customers customers = xmlService.getCustomersByCityByFileType(file, city);
        return responseEntityService.getCustomersResponseEntity(customers);
    }

    @PostMapping("/customers")
    public ResponseEntity<Customers> createCustomer(@RequestParam("file") String file, @RequestBody Customers customers) throws IllegalAccessException {

        xmlService.saveXmlFileToDisc(file, customers);
        return responseEntityService.getCustomersResponseEntity(customers);
    }

    @PutMapping("/customers")
    public ResponseEntity<Customers> updateCustomer(@RequestParam("file") String file, @RequestBody Customers customers) throws IllegalAccessException {

        xmlService.updateXmlFile(file, customers);
        return responseEntityService.getCustomersResponseEntity(customers);
    }

    @DeleteMapping("/customers")
    public ResponseEntity<Void> removeFile(@RequestParam("fileBasePath") String fileBasePath, @RequestParam("file") String file) {
        Boolean fileDeleted = deleteServices.deleteFile(fileBasePath, file);
        return responseEntityService.deleteFilesResponseEntity(fileDeleted);
    }

}
