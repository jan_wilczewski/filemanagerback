package XmlWithSpring.xml;
import XmlWithSpring.model.Customers;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

@Component
public class XmlWriter {

    private final static Logger logger = Logger.getLogger(XmlWriter.class);

    void saveItemsListToXmlFile(String filePath, Customers customers){

        try {
            File file = new File(filePath);

            JAXBContext jaxbContext = JAXBContext.newInstance(Customers.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(customers, file);
            jaxbMarshaller.marshal(customers, System.out);

            if (logger.isInfoEnabled()){
                logger.info("This is INFO log saving a file.");
            }

        } catch (JAXBException e) {
            logger.error("This is logger error:" + e);
        }
    }
}
