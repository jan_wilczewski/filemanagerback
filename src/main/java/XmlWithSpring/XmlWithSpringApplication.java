package XmlWithSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"XmlWithSpring", "XmlWithSpring"})
public class XmlWithSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlWithSpringApplication.class, args);
	}
}
