package XmlWithSpring.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class DeleteServicesTest {

    public String fileName;
    public String filePath;
    public DeleteServices deleteServices = new DeleteServices();


    @Test
    public void deleteFile() throws Exception {

        fileName = "88.xml";
        filePath = "XmlFileBase\\";

        assertTrue(deleteServices.deleteFile(filePath, fileName));
    }

    @Test
    public void deleteFileDoesNotFindTheRequestedFile() {
        fileName = "99.xml";
        filePath = "XmlFileBase\\";

        assertTrue(!deleteServices.deleteFile(filePath, fileName));

    }

}