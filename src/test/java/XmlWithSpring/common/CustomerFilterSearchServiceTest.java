package XmlWithSpring.common;

import XmlWithSpring.model.Customer;
import XmlWithSpring.model.Customers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerFilterSearchServiceTest {

    public Customer customer1 = new Customer();
    public Customer customer2 = new Customer();
    public List<Customer> customerList = new ArrayList<>();
    public Customers expectedCustomers = new Customers();
    public List<Customer> resultList = new ArrayList<>();
    public Customers resultCustomers = new Customers();

    CustomerFilterSearchService customerFilterSearchService = new CustomerFilterSearchService();


    @Before
    public void setUp() throws Exception {
        customer1.setName("Jan");
        customer1.setLastName("Wilk");
        customer1.setAge(33);
        customer1.setCity("Poznan");
        customer1.setPhone(111222333);
        customer1.setEmail("jan@g.com");

        customer2.setName("Adam");
        customer2.setLastName("Adam");
        customer2.setAge(22);
        customer2.setCity("Gdynia");
        customer2.setPhone(333222111);
        customer2.setEmail("adam@g.com");

        customerList.add(customer1);
        customerList.add(customer2);
        expectedCustomers.setCustomerList(customerList);

        resultList.add(customer2);
        resultCustomers.setCustomerList(resultList);

    }

    @Test
    public void getCustomerByName() {
        String name = "Jan";
        assertEquals(customer1, customerFilterSearchService.getCustomerByName(name, expectedCustomers));
    }

    @Test
    public void getCustomerByNameFails() {
        String name = "Domino";
        assertNull(name, customerFilterSearchService.getCustomerByName(name, expectedCustomers));
    }

    @Test
    public void getCustomersByCity() throws Exception {
        String city = "Gdynia";
        assertEquals(resultCustomers, customerFilterSearchService.getCustomersByCity(city, expectedCustomers));
    }

}